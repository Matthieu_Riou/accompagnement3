#include "fonction_math.hpp"
#include <cmath>

double carre(double n)
{
    return puissance(n, 2);
}

double puissance(double n, double p)
{
    return pow(n,p);
}

double racine(double n)
{
    return sqrt(n);
}
